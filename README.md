# git-learning

this is sample git learning project


# To clone Project

git clone [https_url_of_repository]


# To switch between branch

git checkout [branch_name]

# To Pull latest changes from repository

git pull origin [branch_name]

# To add new and modified files to git staging area

git add --all (this will add all files)

git add [file_path] (this will add specific file to git staging area)

# To commit changes to git staging area

git commit -m "message you want to write about task update"

# To push changes to remote branch

git push origin [branch_name]

# To set git local user

git config --local user.name "Xxxxx Ddddd"

git config --local user.email "xxxxxxx@xxxxxx.com"

# Important Notes to take care always

- always take pull before starting work (git pull origin [branch_name])
- always do code in development branch
- always do git add AND git commit before taking pull
- confirm with your teammates, don't work on same file at the same time.
- always take care of gitignore, add node modules, etc files to gitignore.


# HTTP Access Denied Error
 - run below command as Administrator User.
 - git config --system --unset credential.helper
